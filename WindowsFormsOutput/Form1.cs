﻿using OuputFormatLib;
using System;
using System.Windows.Forms;

namespace WindowsFormsOutput
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void GetNameAlertButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                new OutputFormat().GetFormatString(NameTextBox.Text), "Name alert", MessageBoxButtons.OK);
        }
    }
}
