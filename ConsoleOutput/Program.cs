﻿using OuputFormatLib;
using System;

namespace ConsoleOutput
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(new OutputFormat().GetFormatString(args[0]));
        }
    }
}